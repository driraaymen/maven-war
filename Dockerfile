FROM tomcat
COPY target/my-app.war /usr/local/tomcat/webapps/my-app.war
COPY target/my-app/ /usr/local/tomcat/webapps/my-app
EXPOSE 8089